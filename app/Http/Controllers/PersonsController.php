<?php

namespace App\Http\Controllers;

use App\Models\PersonModel;
use Illuminate\Http\Request;

/**
 * @referencia
 * 		https://laravel.com/docs/5.3/controllers#resource-controllers
 */
class PersonsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return response()->view('persons.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
		return response()->view('persons.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function edit(PersonModel $personModel) {
		//
		return response()->view('persons.form');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, PersonModel $personModel) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(PersonModel $personModel) {
		//
	}

}
