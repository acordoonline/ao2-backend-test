<p align="center"><img src="//acordoonline.com/sobre/wp-content/uploads/2020/05/AcordoOnline.png"></p>


## Acordo Online - Teste Dev Backend

* PHP 7.2+
* Laravel 5.3

## Dependencias

* Docker
* Docker Compose

## Instalando o Projeto

Construir as imagens do container

```sh
docker-compose build
```

Iniciar o container

```sh
docker-compose up
```

Acessar o container principal

```sh
docker-compose exec ao_backend_test /bin/bash
```

Instalar as dependências do npm e composer

```sh
npm install && composer install
```

Instalar os migrations

```sh
php artisan migrate
```

O site poderá ser acessado na URL [http://localhost:5000/public/](http://localhost:5000/public/)

## Teste

Sistema CRUD (listagem, criação, edição e remoção).

### Informações

**URL do teste:**
http://localhost:5000/public/persons

**Controller:**
./app/Http/Controllers/**PersonsController.php**

**Model:**
./app/Models/**PersonModel.php**

**Views:**
./resources/views/persons/*****