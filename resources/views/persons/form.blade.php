<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
		@include('_partials.head')
    </head>
    <body>

		<main class="container">

			@include('persons._header')

			<!-- Person List-->
			<div class="my-3 p-3 bg-body rounded shadow-sm">
				<h6 class="border-bottom pb-2 mb-0">[[ Adcionar ou Editar ]]</h6>

				<!-- Formulário --> 
				<form onsubmit="alert('enviar formulario'); return false;">
					<div class="lh-sm border-bottom mb-3">

						<div class="mb-3">
							<label for="username" class="form-label">Usuário</label>
							<div class="input-group mb-3">
								<span class="input-group-text" id="basic-addon1">@</span>
								<input type="text" class="form-control" id="username" aria-describedby="username-help">
							</div>
							<div id="username-help" class="form-text">Nome do usuário.</div>
						</div>


						<div class="p-4 text-center text-muted">
							[[ campos restantes ]]
						</div>

					</div>
					<div class="text-end">
						<button type="submit" class="btn btn-sm btn-primary">Salvar Alterações</button>
					</div>
				</form>

			</div>
		</main>
	</body>
</html>
