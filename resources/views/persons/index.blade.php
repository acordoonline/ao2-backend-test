<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
		@include('_partials.head')
    </head>
    <body>

		<main class="container">

			@include('persons._header')

			<!-- Person List-->
			<div class="my-3 p-3 bg-body rounded shadow-sm">
				<h6 class="border-bottom pb-2 mb-0">Cadastros</h6>

				<!--
					Lista de pessoas cadastradas
				--> 
				<div class="lh-sm border-bottom">
					<div class="d-flex text-muted pt-3">
						<img class="flex-shrink-0 me-2 rounded" src="//via.placeholder.com/32x32.png?text=AO" width="32" height="32" />
						<p class="pb-3 mb-0 small">
							<a href="#editar"><strong class="d-block text-gray-dark">@username &lt;email@example.com&gt;</strong></a>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
							dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</p>
					</div>
					<div class="text-muted pt-1">
						<p class="small text-end">
							01/01/2020 | 
							<button type="button" class="btn btn-sm btn-danger" onclick="alert('Excluir');"><i class="bi bi-trash small"></i></button>
						</p>
					</div>
				</div>


				<div class="d-flex justify-content-between mt-3">
					<div></div>
					<div>
						<small class="text-muted text-center">[[ paginação ]]</small>
					</div>
					<small class="text-end">
						<a href="#adicionar">Adicionar</a>
					</small>
				</div>

			</div>

		</main>

    </body>
</html>
